![](/images/Peek_2019-06-17_17-27.gif)

# Android MVVM with mocked integration tests

My way to MVVM using RxJava with new Android databinding and Koin

## Summary
* Use [MVVM][1] using [architecture components][6] with to separate Android Framework with a [clean architecture][2] to my domain logic.
* Use [Android Databinding][3] wih [LiveData][8] to glue [ViewModel][9] and Android
* Asynchronous communications implemented with [Rx][4].
* Rest API from [ComicVine][5]
* Store data using [Room][7] or [DataFlow][10]

## Dependencies
* architecture components
  * livedata
  * viewmodel
* rx-java
* okhttp
* retrofit
* koin
* Image loading
* Tests

TODO LIST
---------
* Add persistent search Ui components
* Coroutines
* Persistence, layer room or dbflow or MMKV

ARCHITECTURE
____________
![](/images/mvvm.png)
![](/images/mvvm_impl.png)


Developed By
------------

Justinas Krutulis - <justinas.krutulis@gmail.com>

<a href="https://www.linkedin.com/in/justinas-krutulis-7707a851/">
  <img alt="Add me to Linkedin" src="/images/linkedin_icon.png" height="128"/>
</a>

Why MVVM
--------

<iframe width="560" height="315" src="https://www.youtube.com/embed/L634o_Rjly0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<div align="center">
  <a href="https://www.youtube.com/watch?v=L634o_Rjly0"><img src="https://img.youtube.com/vi/L634o_Rjly0/0.jpg" alt="MVC vs MVP vs MVVM vs MVI"></a>
</div>

License
-------

    Copyright 2019 Justinas Krutulis
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
    
[1]: https://en.wikipedia.org/wiki/Model_View_ViewModel
[2]: http://blog.8thlight.com/uncle-bob/2012/08/13/the-clean-architecture.html
[3]: https://developer.android.com/topic/libraries/data-binding/index.html
[4]: http://reactivex.io/
[5]: http://www.comicvine.com/api/
[6]: https://developer.android.com/topic/libraries/architecture/index.html
[7]: https://developer.android.com/topic/libraries/architecture/room.html
[8]: https://developer.android.com/topic/libraries/architecture/livedata.html
[9]: https://developer.android.com/topic/libraries/architecture/viewmodel.html
[10]: https://github.com/agrosner/DBFlow/tree/develop
[11]: https://github.com/Tencent/MMKV/tree/master/Android/MMKV/gradle
[12]: https://github.com/android10/Android-CleanArchitecture-Kotlin
[13]: https://github.com/SmartDengg/android-easy-clean-architecture-boilerplate
[14]: https://github.com/bufferapp/android-clean-architecture-boilerplate/tree/master/remote/src/test/java/org/buffer/android/boilerplate/remote
[15]: https://rickandmortyapi.com/
[16]: https://github.com/googlesamples/android-architecture-components/blob/master/GithubBrowserSample/app/src/test/java/com/android/example/github/api/GithubServiceTest.kt
[17]: https://github.com/evant/PokeMVVM
[18]: https://material.io/design/components/lists.html#specs