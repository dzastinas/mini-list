package net.justinas.rickapiwrapper

class CommaList<T>(private val list: List<T>) {

    override fun toString(): String = list.joinToString(separator = ",")
}
