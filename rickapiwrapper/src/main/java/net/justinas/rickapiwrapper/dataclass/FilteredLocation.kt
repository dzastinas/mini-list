package net.justinas.rickapiwrapper.dataclass

data class FilteredLocation(val info: PageInfo = PageInfo(), val results: List<Location> = emptyList(), val error: String = "")
