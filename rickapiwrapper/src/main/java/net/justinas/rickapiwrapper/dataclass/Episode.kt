package net.justinas.rickapiwrapper.dataclass


data class Episode(
    val id: Long = 0,
    val name: String = "",
    val air_date: String = "",
    val episode: String,
    val characters: List<String>,
    val url: String = "",
    val created: String
)