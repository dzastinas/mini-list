package net.justinas.rickapiwrapper.dataclass


data class CharacterInfo(
    var image: String = "",
    var gender: String = "",
    var species: String = "",
    var created: String = "",
    var origin: Origin = Origin(),
    var name: String = "",
    var location: Location = Location(),
    var episode: List<String> = emptyList(),
    var id: Long = 0,
    var type: String = "",
    var url: String = "",
    var status: String = ""
) {
    data class Origin(val name: String = "", val url: String = "")
}