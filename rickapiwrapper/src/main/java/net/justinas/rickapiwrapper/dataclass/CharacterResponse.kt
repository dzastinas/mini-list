package net.justinas.rickapiwrapper.dataclass


data class CharacterResponse(val info: PageInfo = PageInfo(), val results: List<CharacterInfo> = emptyList(), val error: String = "")


