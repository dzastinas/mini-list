package net.justinas.rickapiwrapper.dataclass

data class Location(
    val id: Long = 0,
    val name: String = "",
    val type: String = "",
    val dimension: String = "",
    val residents: List<String> = emptyList(),
    val url: String = "",
    val created: String = ""
) {
    constructor(name: String, url: String) : this(0, name = name, url = url)
}