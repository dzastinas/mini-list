package net.justinas.rickapiwrapper.dataclass

data class FilteredEpisode(val info: PageInfo = PageInfo(), val results: List<Episode> = emptyList(), val error: String = "")
