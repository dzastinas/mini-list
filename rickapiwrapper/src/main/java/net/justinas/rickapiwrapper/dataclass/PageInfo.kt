package net.justinas.rickapiwrapper.dataclass

data class PageInfo(val count: Int = 0, val pages: Int = 0, val next: String = "", val prev: String = "")