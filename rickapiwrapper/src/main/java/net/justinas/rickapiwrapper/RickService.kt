package net.justinas.rickapiwrapper

import io.reactivex.Single
import net.justinas.rickapiwrapper.dataclass.CharacterInfo
import net.justinas.rickapiwrapper.dataclass.CharacterResponse
import net.justinas.rickapiwrapper.dataclass.Episode
import net.justinas.rickapiwrapper.dataclass.FilteredEpisode
import net.justinas.rickapiwrapper.dataclass.FilteredLocation
import net.justinas.rickapiwrapper.dataclass.Location
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface RickService {
    companion object {
        const val CHARACTER_LIST_URL = "/api/character"
        const val CHARACTER_URL = "/api/character/{id}"
        const val LOCATION_LIST_URL = "/api/location"
        const val LOCATION_URL = "/api/location/{id}"
        const val EPISODE_LIST_URL = "/api/episode"
        const val EPISODE_URL = "/api/episode/{id}"
    }

    @GET(CHARACTER_LIST_URL)
    fun getCharacterList(): Single<CharacterResponse>

    @GET(CHARACTER_URL)
    fun getCharacter(@Path("id") id: Number): Single<CharacterInfo>

    @GET(CHARACTER_URL)
    fun searchCharacters(@Path("id") id: CommaList<Number>): Single<List<CharacterInfo>>

    @GET(CHARACTER_LIST_URL)
    fun searchCharacters(@Query("page") page: Number? = null, @Query("name") name: String? = null, @Query("status") status: String? = null): Single<CharacterResponse>

    @GET(LOCATION_URL)
    fun getLocation(@Path("id") id: Number): Single<Location>

    @GET(LOCATION_URL)
    fun getLocations(): Single<List<Location>>

    @GET(LOCATION_URL)
    fun searchLocations(@Path("id") id: CommaList<Number>): Single<List<Location>>

    @GET(LOCATION_LIST_URL)
    fun searchLocations(@Query("page") page: Number? = null, @Query("name") name: String? = null, @Query("type") type: String? = null): Single<FilteredLocation>

    @GET(EPISODE_URL)
    fun getEpisode(@Path("id") id: Number): Single<Episode>

    @GET(EPISODE_URL)
    fun getEpisodes(@Path("id") id: CommaList<Number>): Single<List<Episode>>

    @GET(EPISODE_LIST_URL)
    fun searchEpisodes(@Query("page") page: Number? = null, @Query("name") name: String? = null, @Query("type") type: String? = null): Single<FilteredEpisode>
}