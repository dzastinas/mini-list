package net.justinas.rickapiwrapper

import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okio.Okio


fun enqueueResponse(
    fileName: String,
    mockWebServer: MockWebServer,
    headers: Map<String, String> = emptyMap()
) {
    @Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
    val inputStream = object {}.javaClass.classLoader.getResourceAsStream("test-data/$fileName")
    val source = Okio.buffer(Okio.source(inputStream))
    val mockResponse = MockResponse()
    for ((key, value) in headers) {
        mockResponse.addHeader(key, value)
    }
    mockWebServer.enqueue(
        mockResponse
            .setBody(source.readString(Charsets.UTF_8))
    )
}