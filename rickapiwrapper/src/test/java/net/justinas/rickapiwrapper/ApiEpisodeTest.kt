package net.justinas.rickapiwrapper

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import okhttp3.mockwebserver.MockWebServer
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.Arrays

@RunWith(JUnit4::class)
class ApiEpisodeTest {
    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var service: RickService

    private lateinit var mockWebServer: MockWebServer

    @Before
    fun createService() {
        mockWebServer = MockWebServer()
        service = Retrofit.Builder()
            .baseUrl(mockWebServer.url("/"))
            .addConverterFactory(MoshiConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
            .create(RickService::class.java)
    }

    @After
    fun stopService() {
        mockWebServer.shutdown()
    }

    @Test
    fun `get episode 1`() {
        enqueueResponse("episode_1.json", mockWebServer)
        val episode = service.getEpisode(1).blockingGet()

        val request = mockWebServer.takeRequest()
        assertThat(request.path, `is`("/api/episode/1"))

        assertThat(episode.id, `is`<Long>(1))
        assertThat(episode.name, `is`("Pilot"))
        assertThat(episode.air_date, `is`("December 2, 2013"))
        assertThat(episode.episode, `is`("S01E01"))
        assertThat(
            episode.characters, `is`(
                Arrays.asList(
                    "https://rickandmortyapi.com/api/character/1",
                    "https://rickandmortyapi.com/api/character/2",
                    "https://rickandmortyapi.com/api/character/35",
                    "https://rickandmortyapi.com/api/character/38",
                    "https://rickandmortyapi.com/api/character/62",
                    "https://rickandmortyapi.com/api/character/92",
                    "https://rickandmortyapi.com/api/character/127",
                    "https://rickandmortyapi.com/api/character/144",
                    "https://rickandmortyapi.com/api/character/158",
                    "https://rickandmortyapi.com/api/character/175",
                    "https://rickandmortyapi.com/api/character/179",
                    "https://rickandmortyapi.com/api/character/181",
                    "https://rickandmortyapi.com/api/character/239",
                    "https://rickandmortyapi.com/api/character/249",
                    "https://rickandmortyapi.com/api/character/271",
                    "https://rickandmortyapi.com/api/character/338",
                    "https://rickandmortyapi.com/api/character/394",
                    "https://rickandmortyapi.com/api/character/395",
                    "https://rickandmortyapi.com/api/character/435"
                )
            )
        )
        assertThat(episode.url, `is`("https://rickandmortyapi.com/api/episode/1"))
        assertThat(episode.created, `is`("2017-11-10T12:56:33.798Z"))
    }

    @Test
    fun `get episode 31`() {
        enqueueResponse("episode_31.json", mockWebServer)
        val episode = service.getEpisode(31).blockingGet()

        val request = mockWebServer.takeRequest()
        assertThat(request.path, `is`("/api/episode/31"))

        assertThat(episode.id, `is`<Long>(31))
        assertThat(episode.name, `is`("The Rickchurian Mortydate"))
        assertThat(episode.air_date, `is`("October 1, 2017"))
        assertThat(episode.episode, `is`("S03E10"))
        assertThat(
            episode.characters, `is`(
                Arrays.asList(
                    "https://rickandmortyapi.com/api/character/1",
                    "https://rickandmortyapi.com/api/character/2",
                    "https://rickandmortyapi.com/api/character/3",
                    "https://rickandmortyapi.com/api/character/4",
                    "https://rickandmortyapi.com/api/character/5",
                    "https://rickandmortyapi.com/api/character/13",
                    "https://rickandmortyapi.com/api/character/30",
                    "https://rickandmortyapi.com/api/character/166",
                    "https://rickandmortyapi.com/api/character/244",
                    "https://rickandmortyapi.com/api/character/247",
                    "https://rickandmortyapi.com/api/character/269",
                    "https://rickandmortyapi.com/api/character/335",
                    "https://rickandmortyapi.com/api/character/347",
                    "https://rickandmortyapi.com/api/character/493"
                )
            )
        )
        assertThat(episode.url, `is`("https://rickandmortyapi.com/api/episode/31"))
        assertThat(episode.created, `is`("2017-11-10T12:56:36.929Z"))
    }

    @Test
    fun `get episode 1-20`() {
        enqueueResponse("episodes_1-20.json", mockWebServer)
        val episode = service.getEpisodes(CommaList(listOf(1, 20))).blockingGet()

        val request = mockWebServer.takeRequest()
        assertThat(request.path, `is`("/api/episode/1,20"))

        val episode1 = episode[0]
        assertThat(episode1.id, `is`<Long>(1))
        assertThat(episode1.name, `is`("Pilot"))
        assertThat(episode1.air_date, `is`("December 2, 2013"))
        assertThat(episode1.episode, `is`("S01E01"))
        assertThat(episode1.characters.size, `is`(19))
        assertThat(episode1.url, `is`("https://rickandmortyapi.com/api/episode/1"))
        assertThat(episode1.created, `is`("2017-11-10T12:56:33.798Z"))

        val episode2 = episode[1]
        assertThat(episode2.id, `is`<Long>(20))
        assertThat(episode2.name, `is`("Look Who's Purging Now"))
        assertThat(episode2.air_date, `is`("September 27, 2015"))
        assertThat(episode2.episode, `is`("S02E09"))
        assertThat(episode2.characters.size, `is`(10))
        assertThat(episode2.url, `is`("https://rickandmortyapi.com/api/episode/20"))
        assertThat(episode2.created, `is`("2017-11-10T12:56:35.772Z"))
    }

    @Test
    fun `get episode filter name earth`() {
        enqueueResponse("episodes_filter_name-earth.json", mockWebServer)
        val episode = service.searchEpisodes(name = "earth").blockingGet()

        val request = mockWebServer.takeRequest()
        assertThat(request.path, `is`("/api/episode?name=earth"))

        assertThat(episode.error, `is`("There is nothing here"))
    }

    @Test
    fun `get episode filter name earth type planet`() {
        enqueueResponse("episodes_filter_name-earth_type-planet.json", mockWebServer)
        val episode = service.searchEpisodes(name = "earth", type = "planet").blockingGet()

        val request = mockWebServer.takeRequest()
        assertThat(request.path, `is`("/api/episode?name=earth&type=planet"))

        assertThat(episode.error, `is`("There is nothing here"))
    }

    @Test
    fun `get episode first page`() {
        enqueueResponse("episodes_first-page.json", mockWebServer)
        val episodeList = service.searchEpisodes(1).blockingGet()

        val request = mockWebServer.takeRequest()
        assertThat(request.path, `is`("/api/episode?page=1"))

        assertThat(episodeList.error, `is`(""))
        val info = episodeList.info

        assertThat(info.pages, `is`(2))
        assertThat(info.count, `is`(31))
        assertThat(info.next, `is`("https://rickandmortyapi.com/api/episode/?page=2"))
        assertThat(info.prev, `is`(""))

        val episode = episodeList.results[0]

        assertThat(episode.id, `is`<Long>(1))
        assertThat(episode.name, `is`("Pilot"))
        assertThat(episode.air_date, `is`("December 2, 2013"))
        assertThat(episode.episode, `is`("S01E01"))
        assertThat(episode.characters.size, `is`(19))
        assertThat(episode.url, `is`("https://rickandmortyapi.com/api/episode/1"))
        assertThat(episode.created, `is`("2017-11-10T12:56:33.798Z"))
    }

    @Test
    fun `get episode second page`() {
        enqueueResponse("episodes_second-page.json", mockWebServer)
        val episodeList = service.searchEpisodes(2).blockingGet()

        val request = mockWebServer.takeRequest()
        assertThat(request.path, `is`("/api/episode?page=2"))

        assertThat(episodeList.error, `is`(""))
        val info = episodeList.info

        assertThat(info.pages, `is`(2))
        assertThat(info.count, `is`(31))
        assertThat(info.next, `is`(""))
        assertThat(info.prev, `is`("https://rickandmortyapi.com/api/episode/?page=1"))

        val episode = episodeList.results[0]

        assertThat(episode.id, `is`<Long>(21))
        assertThat(episode.name, `is`("The Wedding Squanchers"))
        assertThat(episode.air_date, `is`("October 4, 2015"))
        assertThat(episode.episode, `is`("S02E10"))
        assertThat(episode.characters.size, `is`(29))
        assertThat(episode.url, `is`("https://rickandmortyapi.com/api/episode/21"))
        assertThat(episode.created, `is`("2017-11-10T12:56:35.875Z"))
    }
}