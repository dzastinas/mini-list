package net.justinas.rickapiwrapper

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import net.justinas.rickapiwrapper.dataclass.CharacterInfo
import net.justinas.rickapiwrapper.dataclass.Location
import okhttp3.mockwebserver.MockWebServer
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.Arrays

@RunWith(JUnit4::class)
class ApiCharacterTest {
    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var service: RickService

    private lateinit var mockWebServer: MockWebServer

    @Before
    fun createService() {
        mockWebServer = MockWebServer()
        service = Retrofit.Builder()
            .baseUrl(mockWebServer.url("/"))
            .addConverterFactory(MoshiConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
            .create(RickService::class.java)
    }

    @After
    fun stopService() {
        mockWebServer.shutdown()
    }

    @Test
    fun `get character 1`() {
        enqueueResponse("character_1.json", mockWebServer)
        val rick = service.getCharacter(1L).blockingGet()

        val request = mockWebServer.takeRequest()
        assertThat(request.path, `is`("/api/character/1"))

        isRick(rick)
    }

    @Test
    fun `get character 66`() {
        enqueueResponse("character_66.json", mockWebServer)
        val character = service.getCharacter(66L).blockingGet()

        val request = mockWebServer.takeRequest()
        assertThat(request.path, `is`("/api/character/66"))

        assertThat(character, notNullValue())
        assertThat(character.created, `is`("2017-11-30T11:10:10.491Z"))
        assertThat(
            character.episode, `is`(
                Arrays.asList(
                    "https://rickandmortyapi.com/api/episode/18"
                )
            )
        )
        assertThat(character.gender, `is`("Male"))
        assertThat(character.id, `is`(66L))
        assertThat(character.image, `is`("https://rickandmortyapi.com/api/character/avatar/66.jpeg"))
        assertThat(
            character.location,
            `is`(Location("Earth (Replacement Dimension)", "https://rickandmortyapi.com/api/location/20"))
        )
        assertThat(character.name, `is`("Coach Feratu (Balik Alistane)"))
        assertThat(
            character.origin,
            `is`(CharacterInfo.Origin("Earth (Replacement Dimension)", "https://rickandmortyapi.com/api/location/20"))
        )
        assertThat(character.species, `is`("Vampire"))
        assertThat(character.status, `is`("Dead"))
        assertThat(character.type, `is`(""))
        assertThat(character.url, `is`("https://rickandmortyapi.com/api/character/66"))
    }

    @Test
    fun `get characters 1-23-4-56`() {
        enqueueResponse("characters_1-23-4-56.json", mockWebServer)
        val response = service.searchCharacters(CommaList(listOf<Number>(1, 23, 4, 56))).blockingGet()

        val request = mockWebServer.takeRequest()
        assertThat(request.path, `is`("/api/character/1,23,4,56"))

        val rick = response[0]
        isRick(rick)

        val two = response[1]

        assertThat(two, notNullValue())
        assertThat(two.created, `is`("2017-11-04T19:22:43.665Z"))
        assertThat(
            two.episode, `is`(
                Arrays.asList(
                    "https://rickandmortyapi.com/api/episode/6",
                    "https://rickandmortyapi.com/api/episode/7",
                    "https://rickandmortyapi.com/api/episode/8",
                    "https://rickandmortyapi.com/api/episode/9",
                    "https://rickandmortyapi.com/api/episode/10",
                    "https://rickandmortyapi.com/api/episode/11",
                    "https://rickandmortyapi.com/api/episode/12",
                    "https://rickandmortyapi.com/api/episode/14",
                    "https://rickandmortyapi.com/api/episode/15",
                    "https://rickandmortyapi.com/api/episode/16",
                    "https://rickandmortyapi.com/api/episode/18",
                    "https://rickandmortyapi.com/api/episode/19",
                    "https://rickandmortyapi.com/api/episode/20",
                    "https://rickandmortyapi.com/api/episode/21",
                    "https://rickandmortyapi.com/api/episode/22",
                    "https://rickandmortyapi.com/api/episode/23",
                    "https://rickandmortyapi.com/api/episode/24",
                    "https://rickandmortyapi.com/api/episode/25",
                    "https://rickandmortyapi.com/api/episode/26",
                    "https://rickandmortyapi.com/api/episode/27",
                    "https://rickandmortyapi.com/api/episode/28",
                    "https://rickandmortyapi.com/api/episode/29",
                    "https://rickandmortyapi.com/api/episode/30",
                    "https://rickandmortyapi.com/api/episode/31"
                )
            )
        )
        assertThat(two.gender, `is`("Female"))
        assertThat(two.id, `is`(4L))
        assertThat(two.image, `is`("https://rickandmortyapi.com/api/character/avatar/4.jpeg"))
        assertThat(
            two.location,
            `is`(Location("Earth (Replacement Dimension)", "https://rickandmortyapi.com/api/location/20"))
        )
        assertThat(two.name, `is`("Beth Smith"))
        assertThat(
            two.origin,
            `is`(CharacterInfo.Origin("Earth (Replacement Dimension)", "https://rickandmortyapi.com/api/location/20"))
        )
        assertThat(two.species, `is`("Human"))
        assertThat(two.status, `is`("Alive"))
        assertThat(two.type, `is`(""))
        assertThat(two.url, `is`("https://rickandmortyapi.com/api/character/4"))

        val three = response[2]

        assertThat(three, notNullValue())
        assertThat(three.created, `is`("2017-11-05T08:43:05.095Z"))
        assertThat(
            three.episode, `is`(
                Arrays.asList(
                    "https://rickandmortyapi.com/api/episode/13",
                    "https://rickandmortyapi.com/api/episode/19",
                    "https://rickandmortyapi.com/api/episode/21",
                    "https://rickandmortyapi.com/api/episode/25",
                    "https://rickandmortyapi.com/api/episode/26"
                )
            )
        )
        assertThat(three.gender, `is`("Male"))
        assertThat(three.id, `is`(23L))
        assertThat(three.image, `is`("https://rickandmortyapi.com/api/character/avatar/23.jpeg"))
        assertThat(
            three.location,
            `is`(Location("Immortality Field Resort", "https://rickandmortyapi.com/api/location/7"))
        )
        assertThat(three.name, `is`("Arcade Alien"))
        assertThat(three.origin, `is`(CharacterInfo.Origin("unknown")))
        assertThat(three.species, `is`("Alien"))
        assertThat(three.status, `is`("unknown"))
        assertThat(three.type, `is`(""))
        assertThat(three.url, `is`("https://rickandmortyapi.com/api/character/23"))

        val four = response[3]

        assertThat(four, notNullValue())
        assertThat(four.created, `is`("2017-11-05T11:34:16.447Z"))
        assertThat(
            four.episode, `is`(
                Arrays.asList(
                    "https://rickandmortyapi.com/api/episode/28"
                )
            )
        )
        assertThat(four.gender, `is`("Male"))
        assertThat(four.id, `is`(56L))
        assertThat(four.image, `is`("https://rickandmortyapi.com/api/character/avatar/56.jpeg"))
        assertThat(
            four.location,
            `is`(Location("Citadel of Ricks", "https://rickandmortyapi.com/api/location/3"))
        )
        assertThat(four.name, `is`("Bootleg Portal Chemist Rick"))
        assertThat(four.origin, `is`(CharacterInfo.Origin("unknown")))
        assertThat(four.species, `is`("Human"))
        assertThat(four.status, `is`("Dead"))
        assertThat(four.type, `is`(""))
        assertThat(four.url, `is`("https://rickandmortyapi.com/api/character/56"))
    }

    @Test
    fun `get characters 1-183`() {
        enqueueResponse("characters_1-183.json", mockWebServer)
        val response = service.searchCharacters(CommaList(listOf<Number>(1, 183))).blockingGet()

        val request = mockWebServer.takeRequest()
        assertThat(request.path, `is`("/api/character/1,183"))

        val rick = response[0]
        isRick(rick)

        val two = response[1]

        assertThat(two, notNullValue())
        assertThat(two.created, `is`("2017-12-29T18:51:29.693Z"))
        assertThat(
            two.episode, `is`(
                Arrays.asList(
                    "https://rickandmortyapi.com/api/episode/8"
                )
            )
        )
        assertThat(two.gender, `is`("Male"))
        assertThat(two.id, `is`(183L))
        assertThat(two.image, `is`("https://rickandmortyapi.com/api/character/avatar/183.jpeg"))
        assertThat(
            two.location,
            `is`(Location("Earth (C-500A)", "https://rickandmortyapi.com/api/location/23"))
        )
        assertThat(two.name, `is`("Johnny Depp"))
        assertThat(two.origin, `is`(CharacterInfo.Origin("Earth (C-500A)", "https://rickandmortyapi.com/api/location/23")))
        assertThat(two.species, `is`("Human"))
        assertThat(two.status, `is`("Alive"))
        assertThat(two.type, `is`(""))
        assertThat(two.url, `is`("https://rickandmortyapi.com/api/character/183"))
    }

    @Test
    fun `get characters fifth-page`() {
        enqueueResponse("characters_fifth-page.json", mockWebServer)
        val response = service.searchCharacters(5).blockingGet()

        val request = mockWebServer.takeRequest()
        assertThat(request.path, `is`("/api/character?page=5"))

        val info = response.info

        assertThat(info.pages, `is`(25))
        assertThat(info.count, `is`(493))
        assertThat(info.next, `is`("https://rickandmortyapi.com/api/character/?page=6"))
        assertThat(info.prev, `is`("https://rickandmortyapi.com/api/character/?page=4"))

        assertThat(response.results.size, `is`(20))
        val character = response.results[0]
        assertIdNameDate(character, "2017-11-30T14:23:41.053Z", 81, "Crocubot")

        val character1 = response.results[1]
        assertIdNameDate(character1, "2017-11-30T14:28:54.596Z", 82, "Cronenberg Rick")
    }

    @Test
    fun `get characters with filter rick alive`() {
        enqueueResponse("characters_filter_name-rick_status-alive.json", mockWebServer)
        val response = service.searchCharacters(name = "rick", status = "alive").blockingGet()

        val request = mockWebServer.takeRequest()
        assertThat(request.path, `is`("/api/character?name=rick&status=alive"))

        val info = response.info

        assertThat(info.pages, `is`(2))
        assertThat(info.count, `is`(21))
        assertThat(info.next, `is`("https://rickandmortyapi.com/api/character/?page=2&name=rick&status=alive"))
        assertThat(info.prev, `is`(""))

        assertThat(response.results.size, `is`(20))
        val character = response.results[0]
        assertIdNameDate(character, "2017-11-04T18:48:46.250Z", 1, "Rick Sanchez")

        val character1 = response.results[1]
        assertIdNameDate(character1, "2017-11-05T11:15:26.044Z", 48, "Black Rick")
    }

    @Test
    fun `get characters first page`() {
        enqueueResponse("characters_first-page.json", mockWebServer)
        val response = service.searchCharacters().blockingGet()

        val request = mockWebServer.takeRequest()
        assertThat(request.path, `is`("/api/character"))

        val info = response.info

        assertThat(info.pages, `is`(25))
        assertThat(info.count, `is`(493))
        assertThat(info.next, `is`("https://rickandmortyapi.com/api/character/?page=2"))
        assertThat(info.prev, `is`(""))

        assertThat(response.results.size, `is`(20))
        isRick(response.results[0])

        val character1 = response.results[1]
        assertIdNameDate(character1, "2017-11-04T18:50:21.651Z", 2, "Morty Smith")
    }

    private fun assertIdNameDate(
        character: CharacterInfo,
        date: String,
        id: Long,
        name: String
    ) {
        assertThat(character.created, `is`(date))
        assertThat(character.id, `is`<Long>(id))
        assertThat(character.name, `is`(name))
    }

    private fun isRick(rick: CharacterInfo) {
        assertThat(rick.created, `is`("2017-11-04T18:48:46.250Z"))
        assertThat(
            rick.episode, `is`(
                Arrays.asList(
                    "https://rickandmortyapi.com/api/episode/1",
                    "https://rickandmortyapi.com/api/episode/2",
                    "https://rickandmortyapi.com/api/episode/3",
                    "https://rickandmortyapi.com/api/episode/4",
                    "https://rickandmortyapi.com/api/episode/5",
                    "https://rickandmortyapi.com/api/episode/6",
                    "https://rickandmortyapi.com/api/episode/7",
                    "https://rickandmortyapi.com/api/episode/8",
                    "https://rickandmortyapi.com/api/episode/9",
                    "https://rickandmortyapi.com/api/episode/10",
                    "https://rickandmortyapi.com/api/episode/11",
                    "https://rickandmortyapi.com/api/episode/12",
                    "https://rickandmortyapi.com/api/episode/13",
                    "https://rickandmortyapi.com/api/episode/14",
                    "https://rickandmortyapi.com/api/episode/15",
                    "https://rickandmortyapi.com/api/episode/16",
                    "https://rickandmortyapi.com/api/episode/17",
                    "https://rickandmortyapi.com/api/episode/18",
                    "https://rickandmortyapi.com/api/episode/19",
                    "https://rickandmortyapi.com/api/episode/20",
                    "https://rickandmortyapi.com/api/episode/21",
                    "https://rickandmortyapi.com/api/episode/22",
                    "https://rickandmortyapi.com/api/episode/23",
                    "https://rickandmortyapi.com/api/episode/24",
                    "https://rickandmortyapi.com/api/episode/25",
                    "https://rickandmortyapi.com/api/episode/26",
                    "https://rickandmortyapi.com/api/episode/27",
                    "https://rickandmortyapi.com/api/episode/28",
                    "https://rickandmortyapi.com/api/episode/29",
                    "https://rickandmortyapi.com/api/episode/30",
                    "https://rickandmortyapi.com/api/episode/31"
                )
            )
        )
        assertThat(rick.gender, `is`("Male"))
        assertThat(rick.id, `is`(1L))
        assertThat(rick.image, `is`("https://rickandmortyapi.com/api/character/avatar/1.jpeg"))
        assertThat(
            rick.location,
            `is`(Location("Earth (Replacement Dimension)", "https://rickandmortyapi.com/api/location/20"))
        )
        assertThat(rick.name, `is`("Rick Sanchez"))
        assertThat(rick.origin, `is`(CharacterInfo.Origin("Earth (C-137)", "https://rickandmortyapi.com/api/location/1")))
        assertThat(rick.species, `is`("Human"))
        assertThat(rick.status, `is`("Alive"))
        assertThat(rick.type, `is`(""))
        assertThat(rick.url, `is`("https://rickandmortyapi.com/api/character/1"))
    }


}