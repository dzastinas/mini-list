package net.justinas.minitemplate.domain.remote

import io.reactivex.Single
import net.justinas.minilist.domain.item.IdEntity
import net.justinas.minilist.domain.item.ListRepository
import net.justinas.minilist.domain.item.PagedResponse
import net.justinas.rickapiwrapper.RickService

class ListLocationRepositoryRemote(val api: RickService) : ListRepository {
    override fun search(page: Number): Single<PagedResponse> {
        return api.searchLocations(page = page).map {
            PagedResponse(
                it.info.count,
                it.info.pages,
                it.info.next,
                it.info.prev,
                list = it.results.map { response ->
                IdEntity(response.id, response.name, response.dimension)
            })
        }
    }

    override fun getItems(page: Number): Single<List<IdEntity>> {
        return api.searchLocations().map {
            it.results.map { response ->
                IdEntity(response.id, response.name, response.dimension)
            }
        }
    }
}
