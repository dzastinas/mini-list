package net.justinas.minitemplate.domain.api.login

data class Server(val name: String, val distance: String)
