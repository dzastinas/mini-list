package net.justinas.minitemplate.domain.api.login

data class Token(val token: String)
