package net.justinas.minitemplate.domain.rick.interactor

import io.reactivex.Single
import net.justinas.minilist.util.InteractorSingle
import net.justinas.minitemplate.domain.rick.CharacterRepository
import net.justinas.minitemplate.domain.rick.EpisodeRepository
import net.justinas.minitemplate.domain.rick.data.CharacterWithEpisodes
import net.justinas.minitemplate.util.toIdEntity

class GetCharacterWithEpisodesInteractor(
    private val characterRepository: CharacterRepository,
    private val episodeRepository: EpisodeRepository
) :
    InteractorSingle<CharacterWithEpisodes, GetCharacterWithEpisodesInteractor.Request>() {

    override fun create(request: Request): Single<CharacterWithEpisodes> {
        return characterRepository.getCharacter(request.id)
            .flatMap { response ->
                val episodeIds = gatherIdList(response.episode)
                when {
                    episodeIds.isEmpty() -> Single.just(CharacterWithEpisodes(response, emptyList()))
                    episodeIds.size == 1 -> episodeRepository.getEpisode(episodeIds.first()).map {
                        CharacterWithEpisodes(response, listOf(it.toIdEntity()))
                    }
                    else -> episodeRepository.getEpisodes(episodeIds).map {
                        CharacterWithEpisodes(response, it)
                    }
                }
            }
    }

    private fun gatherIdList(episode: List<String>): List<Number> {
        return episode.mapNotNull { it.substringAfterLast("/").toIntOrNull() }
    }

    data class Request(val id: Number) : InteractorSingle.Request()
}
