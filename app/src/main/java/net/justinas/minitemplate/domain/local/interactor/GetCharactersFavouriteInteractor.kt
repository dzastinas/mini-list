package net.justinas.minitemplate.domain.local.interactor

import io.reactivex.Single
import net.justinas.minilist.domain.item.IdEntity
import net.justinas.minilist.util.InteractorSingle
import net.justinas.minitemplate.domain.local.CharacterFavouriteRepository
import net.justinas.minitemplate.domain.rick.CharacterRepository

class GetCharactersFavouriteInteractor(
    private val repository: CharacterRepository,
    private val favouriteRepository: CharacterFavouriteRepository
) : InteractorSingle<List<IdEntity>, GetCharactersFavouriteInteractor.Request>() {

    class Request : InteractorSingle.Request()

    override fun create(request: Request): Single<List<IdEntity>> {
        return repository.getCharacters(favouriteRepository.getList())
    }
}
