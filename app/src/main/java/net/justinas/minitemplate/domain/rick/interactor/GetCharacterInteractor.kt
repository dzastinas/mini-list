package net.justinas.minitemplate.domain.rick.interactor

import io.reactivex.Single
import net.justinas.minilist.util.InteractorSingle
import net.justinas.minitemplate.domain.rick.CharacterRepository
import net.justinas.rickapiwrapper.dataclass.CharacterInfo

class GetCharacterInteractor(private val repository: CharacterRepository) :
    InteractorSingle<CharacterInfo, GetCharacterInteractor.Request>() {

    override fun create(request: Request): Single<CharacterInfo> {
        return repository.getCharacter(request.id)
    }

    data class Request(val id: Number) : InteractorSingle.Request()
}
