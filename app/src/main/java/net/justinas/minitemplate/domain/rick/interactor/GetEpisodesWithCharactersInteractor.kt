package net.justinas.minitemplate.domain.rick.interactor

import io.reactivex.Single
import net.justinas.minilist.util.InteractorSingle
import net.justinas.minitemplate.domain.rick.CharacterRepository
import net.justinas.minitemplate.domain.rick.EpisodeRepository
import net.justinas.minitemplate.domain.rick.data.EpisodeWithCharacters
import net.justinas.minitemplate.util.gatherIdList
import net.justinas.minitemplate.util.toIdEntity

class GetEpisodesWithCharactersInteractor(
    private val repository: CharacterRepository,
    private val episodes: EpisodeRepository
) :
    InteractorSingle<EpisodeWithCharacters, GetEpisodesWithCharactersInteractor.Request>() {

    override fun create(request: Request): Single<EpisodeWithCharacters> {
        return episodes.getEpisode(request.id)
            .flatMap { response ->
                val episodeIds = response.characters.gatherIdList()
                when {
                    episodeIds.isEmpty() -> Single.just(EpisodeWithCharacters(response, emptyList()))
                    episodeIds.size == 1 -> repository.getCharacter(episodeIds.first()).flatMap {
                        Single.just(EpisodeWithCharacters(response, listOf(it.toIdEntity())))
                    }
                    else -> repository.getCharacters(episodeIds).flatMap {
                        Single.just(EpisodeWithCharacters(response, it))
                    }
                }
            }
    }

    data class Request(val id: Number) : InteractorSingle.Request()
}
