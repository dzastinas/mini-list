package net.justinas.minitemplate.domain.remote

import io.reactivex.Single
import net.justinas.minilist.domain.item.IdEntity
import net.justinas.minitemplate.domain.rick.EpisodeRepository
import net.justinas.minitemplate.util.toIdEntity
import net.justinas.rickapiwrapper.CommaList
import net.justinas.rickapiwrapper.RickService
import net.justinas.rickapiwrapper.dataclass.Episode

class EpisodeRepositoryRemote(val api: RickService) : EpisodeRepository {

    override fun getEpisodes(listId: List<Number>): Single<List<IdEntity>> {
        val episodes = api.getEpisodes(CommaList(listId))
        return episodes
            .map { list -> list.map { it.toIdEntity() } }
    }

    override fun getEpisode(id: Number): Single<Episode> {
        return api.getEpisode(id)
    }
}
