package net.justinas.minitemplate.domain.remote

import io.reactivex.Single
import net.justinas.minilist.domain.item.IdEntity
import net.justinas.minitemplate.domain.rick.CharacterRepository
import net.justinas.minitemplate.util.toIdEntity
import net.justinas.rickapiwrapper.CommaList
import net.justinas.rickapiwrapper.RickService
import net.justinas.rickapiwrapper.dataclass.CharacterInfo

class CharacterRepositoryRemote(val api: RickService) : CharacterRepository {
    override fun getCharacters(listId: List<Number>): Single<List<IdEntity>> {
        return api.searchCharacters(CommaList(listId)).map { list -> list.map { it.toIdEntity() } }
    }

    override fun getCharacter(id: Number): Single<CharacterInfo> {
        return api.getCharacter(id)
    }
}
