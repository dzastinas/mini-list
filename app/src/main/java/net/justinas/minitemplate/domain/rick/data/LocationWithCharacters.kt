package net.justinas.minitemplate.domain.rick.data

import net.justinas.minilist.domain.item.IdEntity

data class LocationWithCharacters(val location: IdEntity, val list: List<IdEntity>)
