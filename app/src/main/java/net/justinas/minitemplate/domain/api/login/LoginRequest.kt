package net.justinas.minitemplate.domain.api.login

data class LoginRequest(val username: String, val password: String)
