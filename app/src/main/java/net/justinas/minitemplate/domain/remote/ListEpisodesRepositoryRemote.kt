package net.justinas.minitemplate.domain.remote

import io.reactivex.Single
import net.justinas.minilist.domain.item.IdEntity
import net.justinas.minilist.domain.item.ListRepository
import net.justinas.minilist.domain.item.PagedResponse
import net.justinas.rickapiwrapper.RickService

class ListEpisodesRepositoryRemote(val api: RickService) : ListRepository {
    override fun search(page: Number): Single<PagedResponse> {
        return api.searchEpisodes(page = page).map {
            PagedResponse(
                it.info.count,
                it.info.pages,
                it.info.next,
                it.info.prev,
                list = it.results.map { response ->
                IdEntity(response.id, response.name, response.air_date)
            })
        }
    }

    override fun getItems(page: Number): Single<List<IdEntity>> {
        return api.searchEpisodes().map {
            it.results.map { response ->
                IdEntity(response.id, response.name, response.air_date)
            }
        }
    }
}
