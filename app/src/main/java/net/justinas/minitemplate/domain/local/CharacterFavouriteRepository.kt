package net.justinas.minitemplate.domain.local

import io.reactivex.Completable

class CharacterFavouriteRepository {

    fun save(id: Number): Completable {
        return Completable.create {
            mutableSet.add(id)
            it.onComplete()
        }
    }

    fun getList(): List<Number> {
        return mutableSet.toList()
    }

    companion object {
        val mutableSet = mutableSetOf<Number>()
    }
}
