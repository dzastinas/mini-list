package net.justinas.minitemplate.domain.rick.interactor

import io.reactivex.Single
import net.justinas.minilist.domain.item.LocationRepository
import net.justinas.minilist.util.InteractorSingle
import net.justinas.minitemplate.domain.rick.CharacterRepository
import net.justinas.minitemplate.domain.rick.data.LocationWithCharacters
import net.justinas.minitemplate.util.toIdEntity

class GetLocationWithCharactersInteractor(
    private val repository: CharacterRepository,
    private val location: LocationRepository
) :
    InteractorSingle<LocationWithCharacters, GetLocationWithCharactersInteractor.Request>() {

    override fun create(request: Request): Single<LocationWithCharacters> {
        return location.getLocation(request.id)
            .flatMap { response ->
                val episodeIds = response.list.mapNotNull { it.toIntOrNull() }
                when {
                    episodeIds.isEmpty() -> Single.just(LocationWithCharacters(response, emptyList()))
                    episodeIds.size == 1 -> repository.getCharacter(episodeIds.first()).flatMap {
                        Single.just(LocationWithCharacters(response, listOf(it.toIdEntity())))
                    }
                    else -> repository.getCharacters(episodeIds).flatMap {
                        Single.just(LocationWithCharacters(response, it))
                    }
                }
            }
    }

    data class Request(val id: Number) : InteractorSingle.Request()
}
