package net.justinas.minitemplate.domain.rick.data

import net.justinas.minilist.domain.item.IdEntity
import net.justinas.rickapiwrapper.dataclass.CharacterInfo

data class CharacterWithEpisodes(
    val character: CharacterInfo = CharacterInfo(),
    val episodes: List<IdEntity> = emptyList()
)
