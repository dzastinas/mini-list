package net.justinas.minitemplate.domain.api.comics

import com.squareup.moshi.Json

data class VolumeResponse(
    @Json(name = "status_code") var statusCode: Int = 0,
    @Json(name = "error") var error: String?,
    @Json(name = "results") var results: List<ApiVolume>
)

data class SuggestionResponse(
    @Json(name = "status_code") var statusCode: Int = 0,
    @Json(name = "error") var error: String?,
    @Json(name = "results") var results: List<SuggestionVolume>
)

data class SuggestionVolume(
    @Json(name = "name") var name: String
)

data class ApiVolume(
    @Json(name = "name") var name: String,
    @Json(name = "publisher") var apiPublisher: ApiPublisher?,
    @Json(name = "image") var apiImage: ApiImage?
)

data class ApiImage(
    @Json(name = "thumb_url") val url: String
)

data class ApiPublisher(
    @Json(name = "name") val name: String
)
