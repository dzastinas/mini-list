package net.justinas.minitemplate.domain.rick.interactor

import io.reactivex.Single
import net.justinas.minilist.util.InteractorSingle
import net.justinas.minitemplate.domain.rick.EpisodeRepository
import net.justinas.rickapiwrapper.dataclass.Episode

class GetEpisodeInteractor(private val repository: EpisodeRepository) :
    InteractorSingle<Episode, GetEpisodeInteractor.Request>() {

    override fun create(request: Request): Single<Episode> {
        return repository.getEpisode(request.id)
    }

    data class Request(val id: Number) : InteractorSingle.Request()
}
