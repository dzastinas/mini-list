package net.justinas.minitemplate.domain.local.interactor

import io.reactivex.Completable
import net.justinas.minilist.util.InteractorCompletable
import net.justinas.minitemplate.domain.local.CharacterFavouriteRepository

class AddCharacterFavouriteInteractor(private val repository: CharacterFavouriteRepository) :
    InteractorCompletable<AddCharacterFavouriteInteractor.Request>() {

    data class Request(val id: Number) : InteractorCompletable.Request()

    override fun create(request: Request): Completable {
        return repository.save(request.id)
    }
}
