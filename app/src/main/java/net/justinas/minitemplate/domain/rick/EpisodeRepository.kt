package net.justinas.minitemplate.domain.rick

import io.reactivex.Single
import net.justinas.minilist.domain.item.IdEntity
import net.justinas.rickapiwrapper.dataclass.Episode

interface EpisodeRepository {
    fun getEpisodes(listId: List<Number>): Single<List<IdEntity>>
    fun getEpisode(id: Number): Single<Episode>
}
