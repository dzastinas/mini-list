package net.justinas.minitemplate.domain.rick.interactor

import io.reactivex.Single
import net.justinas.minilist.domain.item.IdEntity
import net.justinas.minilist.util.InteractorSingle
import net.justinas.minitemplate.domain.rick.EpisodeRepository

class GetEpisodesInteractor(private val repository: EpisodeRepository) :
    InteractorSingle<List<IdEntity>, GetEpisodesInteractor.Request>() {

    override fun create(request: Request): Single<List<IdEntity>> {
        return repository.getEpisodes(listOf(request.id))
    }

    data class Request(val id: Number) : InteractorSingle.Request()
}
