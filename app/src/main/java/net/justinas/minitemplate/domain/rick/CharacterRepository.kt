package net.justinas.minitemplate.domain.rick

import io.reactivex.Single
import net.justinas.minilist.domain.item.IdEntity
import net.justinas.rickapiwrapper.dataclass.CharacterInfo

interface CharacterRepository {
    fun getCharacters(listId: List<Number>): Single<List<IdEntity>>
    fun getCharacter(id: Number): Single<CharacterInfo>
}
