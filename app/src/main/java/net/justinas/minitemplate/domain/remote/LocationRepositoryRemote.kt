package net.justinas.minitemplate.domain.remote

import io.reactivex.Single
import net.justinas.minilist.domain.item.IdEntity
import net.justinas.minilist.domain.item.LocationRepository
import net.justinas.rickapiwrapper.RickService

class LocationRepositoryRemote(val api: RickService) : LocationRepository {
    override fun getLocation(id: Number): Single<IdEntity> {
        return api.getLocation(id)
            .map { response ->
                IdEntity(
                    response.id,
                    response.name,
                    response.type,
                    response.dimension,
                    list = response.residents.map { it.substringAfterLast("/") }
                )
            }
    }
}
