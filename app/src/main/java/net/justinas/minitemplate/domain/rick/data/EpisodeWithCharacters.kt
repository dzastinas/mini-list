package net.justinas.minitemplate.domain.rick.data

import net.justinas.minilist.domain.item.IdEntity
import net.justinas.rickapiwrapper.dataclass.Episode

data class EpisodeWithCharacters(val response: Episode, val list: List<IdEntity>)
