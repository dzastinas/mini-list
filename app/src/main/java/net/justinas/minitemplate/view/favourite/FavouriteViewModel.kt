package net.justinas.minitemplate.view.favourite

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import net.justinas.minilist.domain.item.IdEntity
import net.justinas.minilist.util.LoadResult
import net.justinas.minitemplate.domain.local.interactor.GetCharactersFavouriteInteractor

class FavouriteViewModel(
    private val getCharactersFavouriteInteractor: GetCharactersFavouriteInteractor
) : ViewModel() {

    var result = MutableLiveData<LoadResult<List<IdEntity>>>()
    var totalList = MutableLiveData<List<IdEntity>>()

    private val disposable = CompositeDisposable()

    init {
        getList()
    }

    fun retry() {
        getList()
    }

    private fun getList() {
        result.postValue(LoadResult.Loading)
        disposable.add(
            getCharactersFavouriteInteractor.execute(GetCharactersFavouriteInteractor.Request())
                .subscribeBy(
                    onSuccess = {
                        result.postValue(LoadResult.Success(it))
                        totalList.postValue(it)
                    },
                    onError = {
                        result.postValue(LoadResult.Error(it))
                    })
        )
    }

    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }
}
