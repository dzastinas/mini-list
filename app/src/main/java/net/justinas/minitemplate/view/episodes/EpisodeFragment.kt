package net.justinas.minitemplate.view.episodes

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import net.justinas.minilist.domain.item.IdEntity
import net.justinas.minilist.view.list.IdLinearEntityAdapter
import net.justinas.minitemplate.R
import net.justinas.minitemplate.databinding.FragmentEpisodeBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class EpisodeFragment : Fragment(), IdLinearEntityAdapter.Callbacks {
    private val viewModel: EpisodeViewModel by viewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val binding = FragmentEpisodeBinding.inflate(layoutInflater, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel
        binding.callback = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.getLong("ID")?.let {
                viewModel.loadEpisode(it)
        }
    }

    override fun onItemClick(view: View, item: IdEntity) {
        Navigation.findNavController(view)
            .navigate(R.id.action_to_characterFragment, Bundle().apply { putLong("ID", item.id) })
    }
}
