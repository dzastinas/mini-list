package net.justinas.minitemplate.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import net.justinas.minilist.databinding.FragmentPagingListBinding
import net.justinas.minilist.domain.item.IdEntity
import net.justinas.minilist.view.list.IdEntityAdapter
import net.justinas.minilist.view.paging.PagingListViewModel
import net.justinas.minitemplate.R
import org.koin.androidx.viewmodel.ext.android.viewModel

class PagingListFragment : Fragment(), IdEntityAdapter.Callbacks {

    private val viewModel: PagingListViewModel by viewModel()

    private lateinit var binding: FragmentPagingListBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        setHasOptionsMenu(true)
        binding = DataBindingUtil.inflate(layoutInflater, R.layout.fragment_paging_list, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel
        binding.callback = this
        return binding.root
    }

    override fun onItemClick(view: View, item: IdEntity) {
        Navigation.findNavController(view).navigate(R.id.action_to_characterFragment, Bundle().apply { putLong("ID", item.id) })
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_main, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_favourite_mark -> {
                Navigation.findNavController(requireView()).navigate(R.id.action_to_character_favourite)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
