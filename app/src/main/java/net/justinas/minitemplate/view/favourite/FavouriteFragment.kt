package net.justinas.minitemplate.view.favourite

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import net.justinas.minilist.domain.item.IdEntity
import net.justinas.minilist.view.list.IdEntityAdapter
import net.justinas.minitemplate.R
import net.justinas.minitemplate.databinding.FragmentFavouriteBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class FavouriteFragment : Fragment(), IdEntityAdapter.Callbacks {

    private val viewModel: FavouriteViewModel by viewModel()

    private lateinit var binding: FragmentFavouriteBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        setHasOptionsMenu(true)
        binding = DataBindingUtil.inflate(layoutInflater, R.layout.fragment_favourite, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel
        binding.callback = this
        return binding.root
    }

    override fun onItemClick(view: View, item: IdEntity) {
        Navigation.findNavController(view)
            .navigate(R.id.action_to_characterFragment, Bundle().apply { putLong("ID", item.id) })
    }
}
