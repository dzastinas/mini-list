package net.justinas.minitemplate.view.locations

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import net.justinas.minilist.domain.item.IdEntity
import net.justinas.minilist.view.list.IdLinearEntityAdapter
import net.justinas.minitemplate.R
import net.justinas.minitemplate.databinding.FragmentLocationBinding
import net.justinas.minitemplate.di.Module
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.qualifier.named

class LocationFragment : Fragment(), IdLinearEntityAdapter.Callbacks {
    private val viewModel: LocationViewModel by viewModel(named(Module.LOCATION.name))

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val binding = FragmentLocationBinding.inflate(layoutInflater, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel
        binding.callback = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.getLong("ID")?.let {
            viewModel.loadLocation(it)
        }
    }

    override fun onItemClick(view: View, item: IdEntity) {
        Navigation.findNavController(view)
            .navigate(R.id.action_to_characterFragment, Bundle().apply { putLong("ID", item.id) })
    }
}
