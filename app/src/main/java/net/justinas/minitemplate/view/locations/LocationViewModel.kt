package net.justinas.minitemplate.view.locations

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import net.justinas.minilist.domain.item.IdEntity
import net.justinas.minilist.util.LoadResult
import net.justinas.minitemplate.domain.rick.data.LocationWithCharacters
import net.justinas.minitemplate.domain.rick.interactor.GetLocationWithCharactersInteractor

class LocationViewModel(private val locationInteractor: GetLocationWithCharactersInteractor) : ViewModel() {

    val result = MutableLiveData<LoadResult<LocationWithCharacters>>()
    val location = MutableLiveData<IdEntity>()
    val characters = MutableLiveData<List<IdEntity>>()
    private val disposable = CompositeDisposable()

    fun loadLocation(id: Number) {
        result.value = LoadResult.Loading
        disposable.add(
            locationInteractor.execute(GetLocationWithCharactersInteractor.Request(id)).subscribeBy(
                onSuccess = {
                    result.postValue(LoadResult.Success(it))
                    location.postValue(it.location)
                    characters.postValue(it.list)
                },
                onError = { result.postValue(LoadResult.Error(it)) })
        )
    }

    override fun onCleared() {
        super.onCleared()
        disposable.dispose()
    }
}
