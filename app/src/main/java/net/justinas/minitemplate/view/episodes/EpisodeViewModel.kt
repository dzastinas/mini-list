package net.justinas.minitemplate.view.episodes

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import net.justinas.minilist.util.LoadResult
import net.justinas.minitemplate.domain.rick.data.EpisodeWithCharacters
import net.justinas.minitemplate.domain.rick.interactor.GetEpisodesWithCharactersInteractor

class EpisodeViewModel(private val characterInteractor: GetEpisodesWithCharactersInteractor) : ViewModel() {

    val result = MutableLiveData<LoadResult<EpisodeWithCharacters>>()
    val episode = MutableLiveData<EpisodeWithCharacters>()
    private val disposable = CompositeDisposable()

    fun loadEpisode(id: Number) {
        result.value = LoadResult.Loading
        disposable.add(
            characterInteractor.execute(GetEpisodesWithCharactersInteractor.Request(id)).subscribeBy(
                onSuccess = {
                    result.postValue(LoadResult.Success(it))
                    episode.postValue(it)
                },
                onError = { result.postValue(LoadResult.Error(it)) })
        )
    }

    override fun onCleared() {
        super.onCleared()
        disposable.dispose()
    }
}
