package net.justinas.minitemplate.view.characters

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import net.justinas.minilist.domain.item.IdEntity
import net.justinas.minilist.util.LoadResult
import net.justinas.minilist.util.SingleLiveEvent
import net.justinas.minitemplate.domain.local.interactor.AddCharacterFavouriteInteractor
import net.justinas.minitemplate.domain.rick.interactor.GetCharacterWithEpisodesInteractor
import net.justinas.rickapiwrapper.dataclass.CharacterInfo
import timber.log.Timber

class CharacterViewModel(
    private val characterInteractor: GetCharacterWithEpisodesInteractor,
    private val addInteractor: AddCharacterFavouriteInteractor
) : ViewModel() {

    val result = MutableLiveData<LoadResult<Boolean>>()
    val character = MutableLiveData<CharacterInfo>()
    val list = MutableLiveData<List<IdEntity>>()
    val locationClicked = SingleLiveEvent<Long>()
    val episodeClicked = SingleLiveEvent<Long>()
    private val disposable = CompositeDisposable()

    fun loadCharacter(id: Number) {
        result.value = LoadResult.Loading
        disposable.add(
            characterInteractor.execute(GetCharacterWithEpisodesInteractor.Request(id)).subscribeBy(
                onSuccess = {
                    result.postValue(LoadResult.Success(true))
                    character.postValue(it.character)
                    list.postValue(it.episodes)
                },
                onError = { result.postValue(LoadResult.Error(it)) })
        )
    }

    fun onLocationClicked(url: String) {
        url.substringAfterLast("/").toLongOrNull()?.let { locationClicked.postValue(it) }
    }

    fun onEpisodeClicked(episodes: List<String>) {
        episodes[0].substringAfterLast("/").toLongOrNull()?.let { episodeClicked.postValue(it) }
    }

    override fun onCleared() {
        super.onCleared()
        disposable.dispose()
    }

    fun saveFavourite() {
        character.value?.let { character ->
            addInteractor.execute(AddCharacterFavouriteInteractor.Request(character.id)).subscribeBy(onError = { Timber.e(it) })
        }
    }
}
