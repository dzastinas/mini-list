package net.justinas.minitemplate.view.extensions

import android.content.Intent
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import java.io.File
import net.justinas.minitemplate.BuildConfig
import net.justinas.minitemplate.R
import timber.log.Timber

fun Fragment.shareImage(imageUri: String?) {
    Glide.with(requireContext())
        .asFile()
        .load(imageUri)
        .listener(object : RequestListener<File> {
            override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<File>?, isFirstResource: Boolean): Boolean {
                Timber.e(e)
                return true
            }

            override fun onResourceReady(resource: File?, model: Any?, target: Target<File>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                val uriForFile = FileProvider.getUriForFile(requireContext(), BuildConfig.APPLICATION_ID + ".fileprovider", resource!!)
                val shareIntent: Intent = Intent().apply {
                    action = Intent.ACTION_SEND
                    putExtra(Intent.EXTRA_STREAM, uriForFile)
                    type = "image/jpeg"
                }
                startActivity(Intent.createChooser(shareIntent, resources.getText(R.string.send_to)))
                return true
            }
        })
        .submit()
}
