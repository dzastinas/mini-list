package net.justinas.minitemplate.view.locations

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import net.justinas.minilist.databinding.FragmentListBinding
import net.justinas.minilist.domain.item.IdEntity
import net.justinas.minilist.view.list.IdLinearEntityAdapter
import net.justinas.minilist.view.list.ListViewModel
import net.justinas.minitemplate.R
import net.justinas.minitemplate.di.Module
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.qualifier.named

class LocationsListFragment : Fragment(), IdLinearEntityAdapter.Callbacks {

    private val viewModel: ListViewModel by viewModel(named(Module.LOCATION.name))

    private lateinit var binding: FragmentListBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        setHasOptionsMenu(true)
        binding = DataBindingUtil.inflate(layoutInflater, R.layout.fragment_list, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel
        binding.callback = this
        return binding.root
    }

    override fun onItemClick(view: View, item: IdEntity) {
        Navigation.findNavController(view)
            .navigate(R.id.action_to_characterFragment, Bundle().apply { putLong("ID", item.id) })
    }
}
