package net.justinas.minitemplate.view.characters

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import net.justinas.minilist.domain.item.IdEntity
import net.justinas.minilist.view.list.IdLinearEntityAdapter
import net.justinas.minitemplate.R
import net.justinas.minitemplate.databinding.FragmentCharacterBinding
import net.justinas.minitemplate.view.extensions.shareImage
import org.koin.androidx.viewmodel.ext.android.viewModel

class CharacterFragment : Fragment(), IdLinearEntityAdapter.Callbacks {
    private val viewModel: CharacterViewModel by viewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        setHasOptionsMenu(true)
        val binding = FragmentCharacterBinding.inflate(layoutInflater, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel
        binding.callback = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.getLong("ID")?.let {
            viewModel.loadCharacter(it)
        }
        viewModel.locationClicked.observe(viewLifecycleOwner, { id ->
            Navigation.findNavController(view).navigate(R.id.action_to_locationFragment, Bundle().apply { putLong("ID", id) })
        })

        viewModel.episodeClicked.observe(viewLifecycleOwner, { id ->
            Navigation.findNavController(view).navigate(R.id.action_to_episodeFragment, Bundle().apply { putLong("ID", id) })
        })
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_character_details, menu)
    }

    override fun onItemClick(view: View, item: IdEntity) {
        Navigation.findNavController(view).navigate(R.id.action_to_episodeFragment, Bundle().apply { putLong("ID", item.id) })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_image_share -> {
                shareImage(viewModel.character.value?.image)
                true
            }
            R.id.action_favourite_mark -> {
                viewModel.saveFavourite()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
