package net.justinas.minitemplate.di

import net.justinas.minilist.domain.item.ListRepository
import net.justinas.minilist.domain.item.LocationRepository
import net.justinas.minitemplate.di.Module.CHARACTER
import net.justinas.minitemplate.di.Module.EPISODE
import net.justinas.minitemplate.di.Module.LOCATION
import net.justinas.minitemplate.domain.local.CharacterFavouriteRepository
import net.justinas.minitemplate.domain.remote.CharacterRepositoryRemote
import net.justinas.minitemplate.domain.remote.EpisodeRepositoryRemote
import net.justinas.minitemplate.domain.remote.ListCharacterRepositoryRemote
import net.justinas.minitemplate.domain.remote.ListEpisodesRepositoryRemote
import net.justinas.minitemplate.domain.remote.ListLocationRepositoryRemote
import net.justinas.minitemplate.domain.remote.ListRepositoryRemote
import net.justinas.minitemplate.domain.remote.LocationRepositoryRemote
import net.justinas.minitemplate.domain.rick.CharacterRepository
import net.justinas.minitemplate.domain.rick.EpisodeRepository
import org.koin.core.qualifier.named
import org.koin.dsl.module

val repositoryModule = module {

    single<ListRepository>(named(CHARACTER.name)) { ListCharacterRepositoryRemote(get()) }
    single<ListRepository>(named(EPISODE.name)) { ListEpisodesRepositoryRemote(get()) }
    single<ListRepository>(named(LOCATION.name)) { ListLocationRepositoryRemote(get()) }
    single<ListRepository> { ListRepositoryRemote(get()) }
    single<CharacterRepository> { CharacterRepositoryRemote(get()) }
    single<LocationRepository> { LocationRepositoryRemote(get()) }
    single<EpisodeRepository> { EpisodeRepositoryRemote(get()) }
    single { CharacterFavouriteRepository() }
}
