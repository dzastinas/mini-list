package net.justinas.minitemplate.di

enum class Module {
    CHARACTER, EPISODE, LOCATION
}
