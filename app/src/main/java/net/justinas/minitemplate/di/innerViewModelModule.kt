import net.justinas.minilist.view.list.ListViewModel
import net.justinas.minilist.view.paging.PagingListViewModel
import net.justinas.minitemplate.di.Module.CHARACTER
import net.justinas.minitemplate.di.Module.EPISODE
import net.justinas.minitemplate.di.Module.LOCATION
import net.justinas.minitemplate.view.characters.CharacterViewModel
import net.justinas.minitemplate.view.episodes.EpisodeViewModel
import net.justinas.minitemplate.view.favourite.FavouriteViewModel
import net.justinas.minitemplate.view.locations.LocationViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module

val innerViewModelModule = module {
    viewModel { CharacterViewModel(get(), get()) }

    viewModel { LocationViewModel(get()) }
    viewModel { EpisodeViewModel(get()) }
    viewModel { PagingListViewModel(get()) }
    viewModel(named(CHARACTER.name)) { ListViewModel(get(named(CHARACTER.name))) }
    viewModel(named(EPISODE.name)) { ListViewModel(get(named(EPISODE.name))) }
    viewModel(named(LOCATION.name)) { ListViewModel(get(named(LOCATION.name))) }
    viewModel { FavouriteViewModel(get()) }
}
