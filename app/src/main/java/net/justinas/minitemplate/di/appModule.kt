package net.justinas.minitemplate.di

import java.io.File
import java.util.concurrent.TimeUnit
import net.justinas.minitemplate.BuildConfig
import net.justinas.rickapiwrapper.RickService
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory

val appModule = module {

    single {
        val timeout: Long = 60
        val cacheSize = (5 * 1024 * 1024).toLong()
        val localCache = Cache(File(androidContext().cacheDir, "offlineCache"), cacheSize)
        val httpClient = OkHttpClient().newBuilder()
            .connectTimeout(timeout, TimeUnit.SECONDS)
            .readTimeout(timeout, TimeUnit.SECONDS)
            .writeTimeout(timeout, TimeUnit.SECONDS)
            .cache(localCache)

        if (BuildConfig.DEBUG) {
            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            httpClient.addInterceptor(loggingInterceptor)
        }
        val control = "Cache-Control"
        httpClient.addNetworkInterceptor { chain ->
            val originalResponse = chain.proceed(chain.request())
            val cacheControl = originalResponse.header(control)
            if (cacheControl == null ||
                cacheControl.contains("no-store") ||
                cacheControl.contains("no-cache") ||
                cacheControl.contains("must-revalidate") ||
                cacheControl.contains("max-age=0")
            ) {
                originalResponse.newBuilder()
                    .removeHeader("Pragma")
                    .header(control, "public, max-age=" + 5000)
                    .build()
            } else {
                originalResponse
            }
        }

        httpClient.addInterceptor { chain ->
            try {
                chain.proceed(chain.request())
            } catch (e: Exception) {
                chain.proceed(
                    chain.request().newBuilder()
                        .removeHeader("Pragma")
                        .header(control, "public, only-if-cached")
                        .build()
                )
            }
        }
        httpClient.build()
    }

    single {
        Retrofit.Builder()
            .client(get())
            .baseUrl(net.justinas.rickapiwrapper.BuildConfig.END_POINT)
            .addConverterFactory(MoshiConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
    }

    single {
        (get() as Retrofit).create(RickService::class.java)
    }
}
