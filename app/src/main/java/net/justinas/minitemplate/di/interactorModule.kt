package net.justinas.minitemplate.di

import net.justinas.minilist.domain.item.GetListItemsInteractor
import net.justinas.minilist.domain.item.GetLocationInteractor
import net.justinas.minilist.domain.item.SearchListItemsInteractor
import net.justinas.minitemplate.di.Module.CHARACTER
import net.justinas.minitemplate.di.Module.EPISODE
import net.justinas.minitemplate.di.Module.LOCATION
import net.justinas.minitemplate.domain.local.interactor.AddCharacterFavouriteInteractor
import net.justinas.minitemplate.domain.local.interactor.GetCharactersFavouriteInteractor
import net.justinas.minitemplate.domain.rick.interactor.GetCharacterInteractor
import net.justinas.minitemplate.domain.rick.interactor.GetCharacterWithEpisodesInteractor
import net.justinas.minitemplate.domain.rick.interactor.GetEpisodeInteractor
import net.justinas.minitemplate.domain.rick.interactor.GetEpisodesWithCharactersInteractor
import net.justinas.minitemplate.domain.rick.interactor.GetLocationWithCharactersInteractor
import org.koin.core.qualifier.StringQualifier
import org.koin.core.qualifier.named
import org.koin.dsl.module

val interactorModule = module {

    single(named(CHARACTER.name)) { GetListItemsInteractor(get(named(CHARACTER.name))) }
    single(named(EPISODE.name)) { GetListItemsInteractor(get(named(EPISODE.name))) }
    single(named(LOCATION.name)) { GetListItemsInteractor(get(named(LOCATION.name))) }
    single { SearchListItemsInteractor(get(StringQualifier(CHARACTER.name))) }
    single { GetCharacterInteractor(get()) }
    single { GetCharacterWithEpisodesInteractor(get(), get()) }
    single { GetEpisodesWithCharactersInteractor(get(), get()) }
    single { GetLocationWithCharactersInteractor(get(), get()) }
    single { GetLocationInteractor(get()) }
    single { GetEpisodeInteractor(get()) }
    single { AddCharacterFavouriteInteractor(get()) }
    single { GetCharactersFavouriteInteractor(get(), get()) }
}
