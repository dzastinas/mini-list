package net.justinas.minitemplate

import android.app.Application
import innerViewModelModule
import net.justinas.minitemplate.di.appModule
import net.justinas.minitemplate.di.interactorModule
import net.justinas.minitemplate.di.repositoryModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import timber.log.Timber
import timber.log.Timber.DebugTree

class MiniApp : Application() {
    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.plant(DebugTree())
        }
        startKoin {
            androidLogger()
            androidContext(this@MiniApp)
            modules(appModule, repositoryModule, innerViewModelModule, interactorModule)
        }
    }
}
