package net.justinas.minitemplate.util.fileprovider

import android.net.Uri
import android.text.TextUtils
import android.webkit.MimeTypeMap
import androidx.core.content.FileProvider
import com.bumptech.glide.load.ImageHeaderParser
import com.bumptech.glide.load.ImageHeaderParser.ImageType
import com.bumptech.glide.load.resource.bitmap.DefaultImageHeaderParser
import java.io.FileInputStream

class ImageTypeFileProvider : FileProvider() {

    private val mImageHeaderParser: ImageHeaderParser = DefaultImageHeaderParser()

    override fun getType(uri: Uri): String? {
        var type = super.getType(uri)
        if (!TextUtils.equals(type, "application/octet-stream")) {
            return type
        }
        try {
            val parcelFileDescriptor = openFile(uri, "r") ?: return type
            type = parcelFileDescriptor.use { parcelFileDescriptor2 ->
                val fileInputStream = FileInputStream(parcelFileDescriptor2.fileDescriptor)
                fileInputStream.use { fileInputStream2 ->
                    val imageType: ImageType = mImageHeaderParser.getType(fileInputStream2)
                    getTypeFromImageType(imageType, type)
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return type
    }

    private fun getTypeFromImageType(imageType: ImageType, defaultType: String?): String? {
        val extension: String = when (imageType) {
            ImageType.GIF -> "gif"
            ImageType.JPEG -> "jpg"
            ImageType.PNG_A, ImageType.PNG -> "png"
            ImageType.WEBP_A, ImageType.WEBP -> "webp"
            else -> return defaultType
        }
        return MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension)
    }
}
