package net.justinas.minitemplate.util

import net.justinas.minilist.domain.item.IdEntity
import net.justinas.rickapiwrapper.dataclass.CharacterInfo
import net.justinas.rickapiwrapper.dataclass.Episode

fun Episode.toIdEntity() = IdEntity(id, name, air_date, episode, list = characters)
fun CharacterInfo.toIdEntity(): IdEntity = IdEntity(id, name, gender, url = image, list = episode)

fun List<String>.gatherIdList(): List<Number> = when {
    isEmpty() -> emptyList()
    else -> mapNotNull { it.substringAfterLast("/").toIntOrNull() }
}
