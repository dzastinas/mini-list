@file:Suppress("UNCHECKED_CAST")

package net.justinas.minitemplate

import org.mockito.Mockito


fun <T> any(): T {
    Mockito.any<T>()
    return uninitialized()
}
private fun <T> uninitialized(): T = null as T