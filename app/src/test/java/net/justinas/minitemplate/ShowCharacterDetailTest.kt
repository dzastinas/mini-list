package net.justinas.minitemplate

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.nhaarman.mockitokotlin2.mock
import io.reactivex.Single
import net.justinas.minilist.util.LoadResult
import net.justinas.minitemplate.domain.rick.data.CharacterWithEpisodes
import net.justinas.minitemplate.domain.rick.interactor.GetCharacterWithEpisodesInteractor
import net.justinas.minitemplate.view.characters.CharacterViewModel
import net.justinas.rickapiwrapper.dataclass.CharacterInfo
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify

class ShowCharacterDetailTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @Test
    @Throws(Exception::class)
    fun `When detailViewModel is created on init Should get character data`() {
        // Given
        val characterInteractor: GetCharacterWithEpisodesInteractor = mock()
        val characterItems = CharacterInfo(id = 1, name = "Rick", gender = "Rick text2")
        `when`(characterInteractor.execute(any())).thenReturn(Single.just(CharacterWithEpisodes(characterItems, emptyList())))

        //When
        val viewModel = CharacterViewModel(characterInteractor, mock()).apply { loadCharacter(1) }

        // Should
        viewModel.result.observeForever {
            val result = (it as? LoadResult.Success)?.data
            assertThat(result, `is`(true))
        }// Should
        viewModel.character.observeForever {
            assertThat(it, `is`(characterItems))
        }
        verify(characterInteractor).execute(any())
    }
}
