package net.justinas.minilist.view.login

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import net.justinas.minilist.domain.user.UserRepository
import net.justinas.minilist.util.SingleLiveEvent

class LoginViewModel(val userRepo: UserRepository) : ViewModel() {

    val disposable = CompositeDisposable()

    val username = MutableLiveData<String>().apply { postValue("tesonet") }
    val password = MutableLiveData<String>().apply { postValue("partyanimal") }

    val successLogin = SingleLiveEvent<Any>()

    val loading = MutableLiveData<Boolean>()

    fun onLoginClick() {
        val username1 = username.value
        val password1 = password.value
        if (!username1.isNullOrBlank() && !password1.isNullOrBlank()) {
            loading.postValue(true)
            disposable.add(
                userRepo.login(username1, password1)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeBy(
                        onComplete = {
                            loading.postValue(false)
                            successLogin.call()
                        },
                        onError = {
                            loading.postValue(false)
                        }
                    ))
        }
    }

    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }
}