package net.justinas.minitemplate.domain.remote

import io.reactivex.Completable
import io.reactivex.Single
import net.justinas.minilist.domain.user.UserRepository
import net.justinas.minitemplate.domain.api.login.Token
import retrofit2.Retrofit

@Suppress("UNUSED_PARAMETER")
class UserRepositoryRemote(retrofit: Retrofit) : UserRepository {

    val api: LoginApi = LoginApi(retrofit)

    class LoginApi(retrofit: Retrofit) {
        fun login(username: String, password: String): Single<Token> {
            return Single.just(Token(""))
        }
    }

    override fun login(username: String, password: String): Completable {
        return api.login(username, password)
            .flatMapCompletable { token ->
                save(token.token)
            }
    }

    override fun save(token: String): Completable {
        return Completable.complete()
    }

    override fun logout(): Completable {
        TODO("not implemented") // To change body of created functions use File | Settings | File Templates.
    }
}
