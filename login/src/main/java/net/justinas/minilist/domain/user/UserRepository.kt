package net.justinas.minilist.domain.user

import io.reactivex.Completable


interface UserRepository {

    fun login(username: String, password: String): Completable
    fun save(token: String): Completable
    fun logout(): Completable
}