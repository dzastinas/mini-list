package net.justinas.minilist.ui

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.nhaarman.mockitokotlin2.mock
import io.reactivex.Single
import net.justinas.minilist.domain.item.GetListItemsInteractor
import net.justinas.minilist.domain.item.IdEntity
import net.justinas.minilist.ui.util.any
import net.justinas.minilist.util.LoadResult
import net.justinas.minilist.view.list.ListViewModel
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.Mockito
import org.mockito.Mockito.`when`

class ShowListTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @Test
    @Throws(Exception::class)
    fun `Give init block Should getList`() {
        // Given
        val listInteractor: GetListItemsInteractor = mock()
        val orderItems = listOf(IdEntity(10, "yes", "tezt"), IdEntity(12, "no", "tezt"))
        `when`(listInteractor.execute(any())).thenReturn(Single.just(orderItems))

        //When
        val viewModel = ListViewModel(listInteractor)

        // Should
        viewModel.result.observeForever {
            val result = (it as? LoadResult.Success)?.data
            MatcherAssert.assertThat(result, `is`(orderItems))
        }
        Mockito.verify(listInteractor).execute(any())
    }
}
