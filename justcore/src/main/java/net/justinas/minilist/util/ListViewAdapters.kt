package net.justinas.minilist.util

import androidx.databinding.BindingAdapter
import androidx.databinding.adapters.ListenerUtil
import androidx.recyclerview.widget.RecyclerView
import net.justinas.minilist.R
import net.justinas.minilist.domain.item.IdEntity
import net.justinas.minilist.view.list.IdEntityAdapter
import net.justinas.minilist.view.list.IdLinearEntityAdapter

object ListViewAdapters {
    @JvmStatic
    @BindingAdapter("listItemAdapter", "idEntityCallback", requireAll = false)
    fun RecyclerView.setReviewAdapter(
        result: LoadResult<List<IdEntity>>?,
        callback: IdLinearEntityAdapter.Callbacks
    ) {
        val productAdapter = IdLinearEntityAdapter(callback)
        adapter = productAdapter

        if (result is LoadResult.Success) {
            productAdapter.items = result.data
        }
    }

    @JvmStatic
    @BindingAdapter("listItemAdapter", "idEntityCallback", requireAll = false)
    fun RecyclerView.setStringAdapter(
        result: List<String>?,
        callback: IdLinearEntityAdapter.Callbacks
    ) {
        val productAdapter = IdLinearEntityAdapter(callback)
        adapter = productAdapter

        if (!result.isNullOrEmpty()) {
            productAdapter.items = result.map { IdEntity(it.toLongOrNull()?:0, it) }
        }
    }


    @JvmStatic
    @BindingAdapter("listItemAdapter", "idEntityCallback", requireAll = false)
    fun RecyclerView.setReviewAdapter(
        result: List<IdEntity>?,
        callback: IdLinearEntityAdapter.Callbacks
    ) {
        val productAdapter = IdLinearEntityAdapter(callback)
        adapter = productAdapter

        if (!result.isNullOrEmpty()) { productAdapter.items = result }
    }

    @JvmStatic
    @BindingAdapter("listItemPagedAdapter", "idEntityPagedCallback", requireAll = false)
    fun RecyclerView.setPagingReviewAdapter(
        result: List<IdEntity>?,
        callback: IdEntityAdapter.Callbacks
    ) {
        if (adapter == null) {
            val idEntityAdapter = IdEntityAdapter(callback)
            adapter = idEntityAdapter
        }

        if (adapter != null && result != null && result.isNotEmpty()) {
            (adapter as IdEntityAdapter).items = result
        }
    }

    @JvmStatic
    @BindingAdapter(value = ["onScrollStateChanged", "onScrolled"], requireAll = false)
    fun setOnScrollListener(view: RecyclerView, scrollStateChanged: OnScrollStateChanged?, scrolled: OnScrolled?) {
        val newValue: RecyclerView.OnScrollListener?
        if (scrollStateChanged == null && scrolled == null) {
            newValue = null
        } else {
            newValue = object : RecyclerView.OnScrollListener() {
                override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                    scrollStateChanged?.onScrollStateChanged(recyclerView, newState)
                }

                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    if (scrolled != null) {
                        scrolled.onScrolled(recyclerView, dx, dy)
                    }
                }
            }
        }
        val oldValue = ListenerUtil.trackListener(view, newValue, R.id.recycleView)
        if (oldValue != null) {
            view.removeOnScrollListener(oldValue)
        }
        if (newValue != null) {
            view.addOnScrollListener(newValue)
        }
    }

    interface OnScrollStateChanged {
        fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int)
    }

    interface OnScrolled {
        fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int)
    }
}
