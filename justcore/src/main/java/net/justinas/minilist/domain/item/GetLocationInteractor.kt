package net.justinas.minilist.domain.item

import io.reactivex.Single
import net.justinas.minilist.util.InteractorSingle

class GetLocationInteractor(private val repository: LocationRepository) :
    InteractorSingle<IdEntity, GetLocationInteractor.Request>() {

    override fun create(request: Request): Single<IdEntity> {
        return repository.getLocation(request.id)
    }

    data class Request(val id: Number) : InteractorSingle.Request()
}