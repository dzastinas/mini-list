package net.justinas.minilist.domain.item

import io.reactivex.Single

interface ListRepository {
    fun getItems(page: Number = 0): Single<List<IdEntity>>
    fun search(page: Number = 0): Single<PagedResponse>
}