package net.justinas.minilist.domain.item

data class PagedResponse(
    val count: Int = 0,
    val pages: Int = 0,
    val next: String = "",
    val prev: String? = "",
    val list: List<IdEntity> = emptyList()
)
