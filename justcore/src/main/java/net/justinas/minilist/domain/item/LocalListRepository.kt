package net.justinas.minilist.domain.item

import io.reactivex.Single

class LocalListRepository : ListRepository {
    override fun search(page: Number): Single<PagedResponse> {
        return Single.just(
            PagedResponse(
                list = listOf(
                    IdEntity(5, "sun", "star"),
                    IdEntity(6, "sun4", "star5")
                )
            ))
    }

    override fun getItems(page: Number): Single<List<IdEntity>> {
        return Single.just(listOf(IdEntity(5, "sun", "star")))
    }
}