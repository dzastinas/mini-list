package net.justinas.minilist.domain.item

import io.reactivex.Single
import net.justinas.minilist.util.InteractorSingle

class GetListItemsInteractor(private val repository: ListRepository): InteractorSingle<List<IdEntity>, GetListItemsInteractor.Request>() {

    override fun create(request: Request): Single<List<IdEntity>> {
        return repository.getItems(request.page)
    }

    data class Request(val page: Int) : InteractorSingle.Request()
}