package net.justinas.minilist.domain.item

import io.reactivex.Single

interface LocationRepository {
    fun getLocation(id: Number): Single<IdEntity>
}