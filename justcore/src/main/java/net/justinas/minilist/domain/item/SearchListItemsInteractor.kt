package net.justinas.minilist.domain.item

import io.reactivex.Single
import net.justinas.minilist.util.InteractorSingle

class SearchListItemsInteractor(private val repository: ListRepository) : InteractorSingle<PagedResponse, SearchListItemsInteractor.Request>() {

    override fun create(request: Request): Single<PagedResponse> {
        return repository.search(request.page)
    }

    data class Request(val page: Int) : InteractorSingle.Request()
}
