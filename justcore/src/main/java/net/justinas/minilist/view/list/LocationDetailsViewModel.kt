package net.justinas.minilist.view.list

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import net.justinas.minilist.domain.item.GetLocationInteractor
import net.justinas.minilist.domain.item.IdEntity
import net.justinas.minilist.util.LoadResult

class LocationDetailsViewModel(private val getLocationInteractor: GetLocationInteractor) : ViewModel() {

    var result = MutableLiveData<LoadResult<IdEntity>>()
    var location = MutableLiveData<IdEntity>()

    private val disposable = CompositeDisposable()

    fun loadCharacter(id: Number) {
        result.value = LoadResult.Loading
        disposable.add(
            getLocationInteractor.execute(GetLocationInteractor.Request(id)).subscribeBy(
                onSuccess = { result.postValue(LoadResult.Success(it))
                    location.postValue(it)
                },
                onError = { result.postValue(LoadResult.Error(it)) })
        )
    }

    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }
}