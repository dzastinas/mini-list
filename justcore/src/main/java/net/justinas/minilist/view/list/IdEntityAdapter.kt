package net.justinas.minilist.view.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import net.justinas.minilist.databinding.GridItemWithImageBinding
import net.justinas.minilist.domain.item.IdEntity
import net.justinas.minilist.util.AutoUpdatableAdapter
import kotlin.properties.Delegates

class IdEntityAdapter(private val callbacks: Callbacks? = null) : RecyclerView.Adapter<IdEntityAdapter.ViewHolder>(),
    AutoUpdatableAdapter
{
    interface Callbacks {
        fun onItemClick(view: View, item: IdEntity)
    }

    var items: List<IdEntity> by Delegates.observable(emptyList()) {
            _, old, new ->
        autoNotify(old, new) { o, n -> o.id == n.id }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = GridItemWithImageBinding.inflate(inflater, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.item = items[position]
        holder.binding.executePendingBindings()
    }

    override fun getItemCount(): Int = items.size

    inner class ViewHolder(val binding: GridItemWithImageBinding) : RecyclerView.ViewHolder(binding.root) {
        init {
            itemView.setOnClickListener { callbacks?.onItemClick(it, items[adapterPosition]) }
        }
    }
}