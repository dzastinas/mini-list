package net.justinas.minilist.view.paging

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import net.justinas.minilist.domain.item.IdEntity
import net.justinas.minilist.domain.item.PagedResponse
import net.justinas.minilist.domain.item.SearchListItemsInteractor
import net.justinas.minilist.util.LoadResult

class PagingListViewModel(private val getListItemsInteractor: SearchListItemsInteractor) : ViewModel() {

    var lastResult = MutableLiveData<LoadResult<PagedResponse>>()

    var next = MutableLiveData<String>()
    var totalList = MutableLiveData<List<IdEntity>>()

    private val disposable = CompositeDisposable()

    init {
        getList()
    }

    fun retry() {
        getList()

    }

    private fun getList() {
        lastResult.postValue(LoadResult.Loading)
        disposable.add(
            getListItemsInteractor.execute(SearchListItemsInteractor.Request(0))
                .subscribeBy(
                    onSuccess = {
                        lastResult.postValue(LoadResult.Success(it))
                        totalList.postValue(it.list)
                        next.postValue(it.next)
                    },
                    onError = {
                        lastResult.postValue(LoadResult.Error(it))
                    })
        )
    }

    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }

    private fun loadMoreItems() {
        val nextPage = next.value?.substringAfterLast("=")?.toIntOrNull()
        if (nextPage != null) {
            lastResult.postValue(LoadResult.Loading)
            disposable.add(
                getListItemsInteractor.execute(SearchListItemsInteractor.Request(nextPage))
                    .subscribeBy(
                        onSuccess = {
                            totalList.postValue(totalList.value?.plus(it.list))
                            next.postValue(it.next)
                            lastResult.postValue(LoadResult.Success(it))
                        },
                        onError = { lastResult.postValue(LoadResult.Error(it)) })
            )
        }
    }

    fun onScrolled(lastVisiblePosition: Int) {
        val totalSize = totalList.value?.size ?: 0
        if (next.value != null && lastResult.value !is LoadResult.Loading && lastVisiblePosition >= totalSize - 1) {
            loadMoreItems()
        }
    }
}
